package sample;

import dev.infrontfinance.hackathon.iodevices.cfgservice.client.model.Config;
import lombok.AllArgsConstructor;
import sample.apiclients.DeviceService;

@AllArgsConstructor
public class ButtonThread extends Thread{

    Config config;
    DeviceService deviceService;

    @Override
    public void run(){
        try {
            System.out.println("Sending Id " + this.config.getConfigId());
            deviceService.executeConfig(this.config);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
