package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import sample.apiclients.ConfigService;
import sample.apiclients.DeviceService;

public class JavaFXMain extends Application {

    private static ConfigService configService;
    private static DeviceService deviceService;
    private static String[] args;

    public void show() {
        launch(args);
    }
    //alignment
    //margin

    public static void setParams(final String[] args, final ConfigService configService, final DeviceService deviceService) {
        JavaFXMain.configService = configService;
        JavaFXMain.deviceService = deviceService;
        JavaFXMain.args = args;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //configService.printConfigs();
        //deviceService.executeAction(UUID.fromString(configService.getFirstConfig().getConfigId()));
        //StackPane root = new StackPane();
        GridPane root = new GridPane();
        //Main prog = new Main();
        Controller cont = new Controller(deviceService, primaryStage, configService);
        //cont.createButton("foo", root);
        cont.loadButtons(root);

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 480, 300));
        primaryStage.show();
    }
}
