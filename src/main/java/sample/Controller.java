package sample;


import dev.infrontfinance.hackathon.iodevices.cfgservice.client.model.Config;
import java.util.ArrayList;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import lombok.AllArgsConstructor;
import sample.apiclients.ConfigService;
import sample.apiclients.DeviceService;

@AllArgsConstructor
public class Controller {
    public static int MAXBUTTONSPERROW =3;
    public static int MAXBUTTONROWS =3;

    DeviceService deviceService;
    Stage primaryStage;
    ConfigService configService;


    public Button createSettingsButton(){
        Button b = new Button();
        b.setText("Settings");
        b.setPrefWidth(800/Controller.MAXBUTTONSPERROW);
        b.setPrefHeight(480/Controller.MAXBUTTONROWS);
        b.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println(b.getText()+": Settings menu opened");
                //open settings menu
                b.setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event) {

                        Label secondLabel = new Label("I'm a Label on new Window");

                        StackPane secondaryLayout = new StackPane();
                        secondaryLayout.getChildren().add(secondLabel);

                        Scene secondScene = new Scene(secondaryLayout, 230, 100);

                        // New window (Stage)
                        Stage newWindow = new Stage();
                        newWindow.setTitle("Second Stage");
                        newWindow.setScene(secondScene);

                        // Set position of second window, related to primary window.
                        newWindow.setX(primaryStage.getX() + 200);
                        newWindow.setY(primaryStage.getY() + 100);

                        newWindow.show();
                    }
                });
            }
        });
        return b;
    }


    public List<Button> createButtonConfig(List<Config> cList){
        System.out.println("loadButtons");

        List<Button> list = new ArrayList<>();
       int i=1;
       for(Config c: cList){
           i++;
           if(i==Controller.MAXBUTTONROWS*Controller.MAXBUTTONSPERROW){
               break;
           }
           Button b = new Button();
           b.setText(c.getName());
           b.setPrefWidth(800/Controller.MAXBUTTONSPERROW);
           b.setPrefHeight(480/Controller.MAXBUTTONROWS);
           b.setOnAction(new EventHandler<ActionEvent>() {
               public void handle(ActionEvent event) {
                   System.out.println(b.getText()+": Hello World!");
                   sendStuffAway(c);
               }
           });
           list.add(b);
       }
        Button settingsB =this.createSettingsButton();
        list.add(settingsB);
        System.out.println("loadButtons");
       return list;
    }

    public void sendStuffAway(Config c) {
        System.out.println("Doing Stuff");
        ButtonThread t = new ButtonThread(c, this.deviceService);
        t.start();
        /*try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

         */
        System.out.println("Doing Stuff done");
    }

    public void loadButtons(GridPane root){
        System.out.println("loadButtons");
        List<Config> cList = configService.getAllConfigs();
        List<Button> bList= createButtonConfig(cList);
        System.out.println("cList length:"+cList.size());
        System.out.println("bList length:"+bList.size());
        arrangeButtonsOnGrid(root,bList);
    }

    public void arrangeButtonsOnGrid(GridPane pane, List<Button> bList){
        //Add Buttons
        this.addImageToButton(bList);
        for(int i=1;(i<=Controller.MAXBUTTONSPERROW*Controller.MAXBUTTONROWS)&&(i<=bList.size());i++){
            pane.add(bList.get(i-1),(i-1)%Controller.MAXBUTTONSPERROW,(i-1)/Controller.MAXBUTTONSPERROW);
            System.out.println("Button"+bList.get(i-1).getText()+" added to Grid. Index is:"+i);
            System.out.println("Row"+(i-1)/Controller.MAXBUTTONSPERROW+" Column:"+ (i-1)%Controller.MAXBUTTONSPERROW);
        }
        System.out.println("grid done");
    }

    public void addImageToButton(List<Button> bList){
        for(int i=0;i<bList.size();i++){
            Image img = new Image(i%5+".png");
            ImageView view= new ImageView(img);
            view.setFitHeight(bList.get(i).getHeight());
            view.setFitWidth(bList.get(i).getWidth());
            bList.get(i).setGraphic(view);
            bList.get(i).setTextAlignment(TextAlignment.LEFT);

        }
    }
    public Button createButton(String label, Pane root){
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {

            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        root.getChildren().add(btn);
        return btn;
    }

}
