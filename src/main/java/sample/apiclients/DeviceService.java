package sample.apiclients;

import static java.util.Objects.isNull;
import static sample.apiclients.HttpClientConfiguration.getGenericDeviceApi;

import dev.infrontfinance.hackathon.iodevices.cfgservice.client.model.Config;
import dev.infrontfinance.hackathon.iodevices.localservice.client.api.DeviceApiApi;
import feign.Feign;
import java.util.UUID;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class DeviceService {

  private final DeviceApiApi deviceApiApi;
  private final Feign.Builder genericFeignBuilder;

  public void executeAction(UUID configID) {
    deviceApiApi.executeAction(configID);
  }

  public void executeConfig(@NonNull Config config) {

    if (isNull(config.getTargets()) || config.getTargets().isEmpty()) {
      executeAction(UUID.fromString(config.getConfigId()));
    } else {
      log.info("We got the following targets: {}",config.getTargets());
      config.getTargets()
          .forEach(target -> {
            log.info("Sending ID {} to {}", config.getConfigId(), target);
            DeviceApiApi genericDeviceApi = getGenericDeviceApi(genericFeignBuilder, target);
            if (!isNull(genericDeviceApi)) {
              log.info("Sending actual request");
              try {
                genericDeviceApi.executeAction(UUID.fromString(config.getConfigId()));
              } catch (Exception e) {
                log.warn("Could not send Action to {}", target, e);
              }
            } else {
              log.info("Got no client");
            }
          });
    }
  }


}
