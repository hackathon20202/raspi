package sample.apiclients;

import dev.infrontfinance.hackathon.iodevices.cfgservice.client.api.ConfigApi;
import dev.infrontfinance.hackathon.iodevices.cfgservice.client.model.Config;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ConfigService {

  private final ConfigApi configApi;

  public void printConfigs() {
    configApi.getAllConfigs().forEach(config -> log.info(config.toString()));
  }

  public Config getFirstConfig() {
    return configApi.getAllConfigs().get(0);
  }

  public List<Config> getAllConfigs() {
    return configApi.getAllConfigs();
  }

}
