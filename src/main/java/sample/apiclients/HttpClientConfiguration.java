package sample.apiclients;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.infrontfinance.hackathon.iodevices.cfgservice.client.api.ConfigApi;
import dev.infrontfinance.hackathon.iodevices.localservice.client.api.DeviceApiApi;
import feign.Client;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class HttpClientConfiguration {

  public static DeviceApiApi getGenericDeviceApi(final Feign.Builder feignBuilder, final String basePath) {
    log.info("Getting deviceApi client to {}", basePath);
    return feignBuilder.target(DeviceApiApi.class, basePath);
  }

  @Bean
  public DeviceApiApi deviceApiApi(final Client client, final ObjectMapper objectMapper) {
    return Feign.builder()
        .client(client)
        .decoder(new JacksonDecoder(objectMapper))
        .encoder(new JacksonEncoder(objectMapper))
        .target(DeviceApiApi.class, "http://172.19.225.31:9000");

  }

  @Bean
  Feign.Builder genericFeignBuilder(final Client client, final ObjectMapper objectMapper) {
    return Feign.builder()
        .client(client)
        .decoder(new JacksonDecoder(objectMapper))
        .encoder(new JacksonEncoder(objectMapper));
  }

  @Bean
  public ConfigApi configApi(final Client client, final ObjectMapper objectMapper) {
    return Feign.builder()
        .client(client)
        .decoder(new JacksonDecoder(objectMapper))
        .encoder(new JacksonEncoder(objectMapper))
        .target(ConfigApi.class, "http://35.192.68.100");

  }


}
