package sample;

import static org.springframework.boot.Banner.Mode.OFF;

import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import sample.apiclients.ConfigService;
import sample.apiclients.DeviceService;

@EnableFeignClients
@SpringBootApplication
@RequiredArgsConstructor
public class MainApplication {

  private static String[] _args;
  private final ConfigService configService;
  private final DeviceService deviceService;


  public static void main(final String[] args) {

    _args = args;
    final SpringApplication app = new SpringApplication(MainApplication.class);
    app.setBannerMode(OFF);
    app.setWebApplicationType(WebApplicationType.NONE);
    app.run(args);
  }


  @PostConstruct
  public void runJavaFXApplication() {
    JavaFXMain javaFXMain = new JavaFXMain();
    javaFXMain.setParams(_args, configService, deviceService);
    javaFXMain.show();
  }
}
